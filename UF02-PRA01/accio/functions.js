var add = (function () {
  var counter = 0;
  return function () {return counter += 1;}
  })();

function clica(event) {
    var x = event.clientX;
    var y = event.clientY;
    var z = add();
    var coor = "X coords: " + x + ", Y coords: " + y;
    create(x, y, z);
}

function create(x, y, z){
  var createDiv = document.getElementById("camp").innerHTML = "<div id='block"+z+"'></div>";
  document.getElementById("block"+z).style.display = "block";
  document.getElementById("block"+z).style.position = "absolute";
  document.getElementById("block"+z).style.transform = "translate(-50%, -50%)";
  document.getElementById("block"+z).style.background = "#FFFFFF";
  document.getElementById("block"+z).style.width = "100px";
  document.getElementById("block"+z).style.height = "100px";
  document.getElementById("block"+z).style.borderRadius = "50px";
  document.getElementById("block"+z).style.top = y + "px";
  document.getElementById("block"+z).style.left = x + "px";
  document.getElementById("block"+z).style.transition = "all 500ms ease-in";
  setTimeout(function(){ moure(z); }, 500);
  }

  function moure(z){
  document.getElementById("block"+z).style.position = "absolute";
  document.getElementById("block"+z).style.display = "block";
  document.getElementById("block"+z).style.transform = "translate(-50%, -50%)";
  document.getElementById("block"+z).style.top = "50%";
  document.getElementById("block"+z).style.left = "50%";

}
