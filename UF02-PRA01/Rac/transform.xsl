<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="channel">
    <html lang="ca-CA">
    <head>
      <title>Rac1 No s'ha parlat prou...</title>
      <meta charset="utf-8"/>
      <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <header>
          <h1></h1>
          <h1 id="right1"></h1>
          <h1 id="rightdown2"></h1>
          <h1 id="down2"></h1>
          <h1 id="right3"></h1>
          <h1 id="rightdown4"></h1>
          <h1 id="down3"></h1>
          <h1 id="right4"></h1>
          <h1 id="rightdown5"></h1>
          <h1 id="down5"></h1>
          </header>
        <section>  <xsl:apply-templates select="item"/></section>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="item">
    <div id="wrap">
      <h2><xsl:value-of select="title"/></h2>
      <p><xsl:value-of select="pubDate"/></p>
      <h3><xsl:value-of select="category"/></h3>
      <p><xsl:value-of select="description"/></p>
        <audio controls="">
          <source>
            <xsl:attribute name="src"><xsl:value-of select="link"/></xsl:attribute>
            <xsl:attribute name="type">audio/mpeg</xsl:attribute>
          </source>
        </audio>
    </div>
  </xsl:template>
</xsl:stylesheet>
